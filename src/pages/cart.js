import React from "react";
import Header from "../components/Header";
import Breadcrumb from "../components/Breadcrumb";
import AsideMenu from "../components/AsideMenu";
import Footer from "../components/Footer";
import { Link } from "react-router-dom";

const Cart = ({ cart, handleRemoveCartItem }) => {
  return (
    <>
      <Header mode="dark" cart={cart} />
      <Breadcrumb />

      <section className="md:py-16">
        <div className="container mx-auto px-4">
          <div className="flex -mx-4 flex-wrap">
            <div
              className="w-full px-4 mb-4 md:w-8/12 md:mb-0"
              id="shopping-cart"
            >
              <div className="flex flex-start mb-4 mt-8 pb-3 border-b border-gray-200 md:border-b-0">
                <h3 className="text-2xl">Shopping Cart</h3>
              </div>

              <div className="border-b border-gray-200 mb-4 hidden md:block">
                <div className="flex flex-start items-center pb-2 -mx-4">
                  <div className="px-4 flex-none">
                    <div className="" style={{ width: "90px" }}>
                      <h6>Photo</h6>
                    </div>
                  </div>
                  <div className="px-4 w-5/12">
                    <div className="">
                      <h6>Product</h6>
                    </div>
                  </div>
                  <div className="px-4 w-5/12">
                    <div className="">
                      <h6>Price</h6>
                    </div>
                  </div>
                  <div className="px-4 w-2/12">
                    <div className="text-center">
                      <h6>Action</h6>
                    </div>
                  </div>
                </div>
              </div>

              {cart && cart.length === 0 && (
                <p id="cart-empty" className=" text-center py-8">
                  Ooops... Cart is empty.{" "}
                  <Link to="/" className="underline">
                    Shop Now
                  </Link>
                </p>
              )}

              {cart.length > 0 &&
                cart.map(function ({ id, item }, index) {
                  return (
                    <div
                      key={index}
                      className="flex flex-start flex-wrap items-center mb-4 -mx-4"
                      data-row="1"
                    >
                      <div className="px-4 flex-none">
                        <div
                          className=""
                          style={{ width: "90px", height: "90px" }}
                        >
                          <img
                            src={item.image1}
                            alt="chair-1"
                            className="object-cover rounded-xl w-full h-full"
                          />
                        </div>
                      </div>
                      <div className="px-4 w-auto flex-1 md:w-5/12">
                        <div className="">
                          <h6 className="font-semibold text-lg md:text-xl leading-8">
                            {item.name}
                          </h6>
                          <span className="text-sm md:text-lg">
                            Office Room
                          </span>
                          <h6 className="font-semibold text-base md:text-lg block md:hidden">
                            {item.price}
                          </h6>
                        </div>
                      </div>
                      <div className="px-4 w-auto flex-none md:flex-1 md:w-5/12 hidden md:block">
                        <div className="">
                          <h6 className="font-semibold text-lg">
                            {item.price}
                          </h6>
                        </div>
                      </div>
                      <div className="px-4 w-2/12">
                        <div className="text-center">
                          <button
                            onClick={(event) => handleRemoveCartItem(event, id)}
                            className="text-red-600 border-none focus:outline-none px-3 py-1"
                          >
                            X
                          </button>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </section>

      <AsideMenu />
      <Footer />
    </>
  );
};

export default Cart;

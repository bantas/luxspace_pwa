import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/Header";
import Hero from "./components/Hero";
import Browse from "./components/Browse";
import Arrived from "./components/Arrived";
import Clients from "./components/Clients";
import AsideMenu from "./components/AsideMenu";
import Footer from "./components/Footer";
import Offline from "./offline";
import Splash from "./pages/splash";
import Profile from "./pages/profile";
import Details from "./pages/details";
import Cart from "./pages/cart";

function App({ cart }) {
  const [items, setItems] = useState();
  const [offlineStatus, setOflineStatus] = useState(!navigator.onLine);
  const [isLoading, setIsLoading] = useState(true);

  const handleOfflineStatus = () => {
    setOflineStatus(!navigator.onLine);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("https://bwacharity.fly.dev/items");
        const { nodes } = await response.json();
        setItems(nodes);

        const script = document.createElement("script");
        script.src = "/carousel.js";
        script.async = false;
        document.body.appendChild(script);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();

    handleOfflineStatus();
    window.addEventListener("online", handleOfflineStatus);
    window.addEventListener("offline", handleOfflineStatus);

    setTimeout(() => {
      setIsLoading(false);
    }, 1500);

    return () => {
      window.removeEventListener("online", handleOfflineStatus);
      window.removeEventListener("offline", handleOfflineStatus);
    };
  }, [offlineStatus]);
  return (
    <>
      {isLoading === true ? (
        <Splash />
      ) : (
        <>
          {offlineStatus && <Offline />}
          <Header mode="light" cart={cart} />
          <Hero />
          <Browse />
          <Arrived items={items} />
          <Clients />
          <AsideMenu />
          <Footer />
        </>
      )}
    </>
  );
}

export default function Routes() {
  const cachedCart = window.localStorage.getItem("cart");
  const [cart, setCart] = useState([]);

  const handleAddToCart = (item) => {
    const currentIndex = cart.length;
    const newCart = [...cart, { id: currentIndex + 1, item }];
    setCart(newCart);
    window.localStorage.setItem("cart", JSON.stringify(newCart));
  };

  const handleRemoveCartItem = (event, id) => {
    const revisiCart = cart.filter(function (item) {
      return item.id != id;
    });
    setCart(revisiCart);
    window.localStorage.setItem("cart", JSON.stringify(revisiCart));
  };

  React.useEffect(
    function () {
      console.info("useEffect for localStorage");
      if (cachedCart !== null) {
        setCart(JSON.parse(cachedCart));
      }
    },
    [cachedCart]
  );
  return (
    <Router>
      <Route path="/" exact>
        <App cart={cart} />
      </Route>
      <Route path="/profile" exact component={Profile} />
      <Route path="/details/:id">
        <Details handleAddToCart={handleAddToCart} cart={cart} />
      </Route>
      <Route path="/cart">
        <Cart cart={cart} handleRemoveCartItem={handleRemoveCartItem} />
      </Route>
    </Router>
  );
}
